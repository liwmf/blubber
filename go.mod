module gerrit.wikimedia.org/r/blubber

require (
	github.com/davecgh/go-spew v1.1.0
	github.com/docker/distribution v2.6.2+incompatible
	github.com/ghodss/yaml v1.0.0
	github.com/go-playground/locales v0.11.2
	github.com/go-playground/universal-translator v0.16.0
	github.com/pborman/getopt v0.0.0-20170112192656-0fd4e972e7f7
	github.com/pmezard/go-difflib v1.0.0
	github.com/stretchr/testify v1.1.4
	github.com/utahta/go-openuri v0.1.0
	gopkg.in/go-playground/validator.v9 v9.12.0
	gopkg.in/yaml.v2 v2.0.0-20170812160011-eb3733d160e7
)
